const express = require('express');
const router = express.Router();
const path = require('path');
var bodyParser = require('body-parser');

var pack_nlg = require("./js/pack_nlg.js");
var render_nlg = require("./js/render_nlg.js");

const app = express();
const port = process.env.PORT || 3000;

const language = 'english'; //english or italian

// Set public folder as root
app.use(express.static('public'));

// Allow front-end access to node_modules folder
app.use('/scripts', express.static(`${__dirname}/node_modules/`));

//add the router
app.use('/', router);

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.post('/saveTemplate', function(req, res){
  //console.log(req.body.arr);
  res.json({ success: true });
  
  pack_nlg.save(req.body.arr, language);
});

app.post('/testTemplate', function(req, res){
  //console.log(req.body.arr);
  
  var response = render_nlg.main(req.body.violation, language);
  //console.log(response);
  
    //console.log(result);
  res.json({ success: response });
  
  //bho(req.body.arr);
  //console.log(req.body.arr);
});

// Listen for HTTP requests on port 3000
app.listen(port, () => {
  console.log('listening on http://localhost:%d', port);
});



