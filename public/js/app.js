var violations = [];

function load() {
   
    //console.log("Page load finished");
    init();

}

function init(){
    readViolations();
    var types =
        [
        {
            "text"  : "Text",
            "selected": true
        },
        {
            "text"     : "Placeholder",
            //"selected" : true
        },
        {
            "text"  : "Rule"
        }
        ];

    
    var templateType = 
        [
            {
                "text"  : "Feedback",
                "selected": true
            },
            {
                "text"  : "Argument"},
            {
                "text"  : "Suggestion"
            }
            
        ];
    var templateBox = document.getElementById('templateType');
    for(i=0; i<templateType.length; i++){
        var option = templateType[i];
        templateBox.options.add(new Option(option.text, option.text, option.selected) );
    }

    
    var typeBoxes = document.getElementsByName('rec_mode');
    for(i=0; i<typeBoxes.length; i++){
        for(j=0; j<types.length; j++){
            var option = types[j];
            typeBoxes[i].options.add(new Option(option.text, option.text, option.selected) );
        }
    }

    
    readTextFile("scheme.json", function(text){
        var data = JSON.parse(text);
        //console.log(data);
        var fields = getFields(data);
        //console.log(fields);

        var selectFields = document.getElementsByName('fields');
        for(i=0; i<selectFields.length; i++){
            for(var j = 0, l = fields.length; j < l; j++){
                var option = fields[j];
                selectFields[i].options.add( new Option(option, option, option) );
            }
        }
    });
  }

  function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
    }

    function getFields(obj){
        var keys = [];
        for(var key in obj){
            keys.push(key);
        }
        return keys;
    }

    

    function cloneRow() {
        var table = document.getElementById("formTable"); // find table to append to
        //var row = document.getElementById("rowToClone"); // find row to copy
        var row = table.rows[1];
        var clone = row.cloneNode(true); // copy children too
        clone.id = "newID"; // change id or other attributes/contents
        table.appendChild(clone); // add new row to end of table
      }
  
function sendData(data){
    $.ajax({
        url: '/saveTemplate',
        type: 'POST',
        data: {
            arr: data
        },
        success: function(resp){
            console.log(resp);
            alert(data[0]+' template saved!')
        }
    });
}


  function addRow() {
    var table = document.getElementById("formTable"); // find table to append to
    //var row = document.getElementById("rowToClone"); // find row to copy
    var utilityTable = document.getElementById('utilityTable');
    var row = utilityTable.rows[1];
    var clone = row.cloneNode(true); // copy children too
    clone.id = "newID"; // change id or other attributes/contents
    var tbody = table.getElementsByTagName('tbody')[0];
    tbody.appendChild(clone); // add new row to end of table
    setPreview();
  }

  function check(object){
    //alert(object);
    console.log(object.value)
    var row = object.parentNode.parentNode;
    console.log(row)
    changeRow(object.value, row)
    setPreview();
}

function changeRow(type, rowToChange) {
    var table = document.getElementById("formTable"); // find table to append to
    var rowToClone
    if(!type.localeCompare('Text')){
        rowToClone = document.getElementById("textRow"); // find row to copy
    }else if(!type.localeCompare('Placeholder')){
        rowToClone = document.getElementById("placeholderRow"); // find row to copy
    }else if(!type.localeCompare('Rule')){
        rowToClone = document.getElementById("ruleRow"); // find row to copy
    }
    //var row = document.getElementById("rowToClone"); // find row to copy
    var clone = rowToClone.cloneNode(true); // copy children too
    clone.id = "newID"; // change id or other attributes/contents
    rowToChange.parentNode.replaceChild(clone, rowToChange);
  }


  function saveTemplate(){
    var table = document.getElementById('formTable');
    var type = document.getElementById('templateType').value;
    
    var array = [];
    array.push(type);
    for (var i = 1; i < table.rows.length ;i++) {
        row = table.rows[i];
        //console.log('here')
        var arr = [];
        var type = row.cells[0].childNodes[1].value;
        arr.push(type);
        if(!type.localeCompare('Text')){
            var value = row.cells[1].children[0].children[0].value;
            arr.push(value);
        }else if(!type.localeCompare('Placeholder')){
            var value = 'violation.'+row.cells[1].childNodes[1].value;
            arr.push(value);
        }else if(!type.localeCompare('Rule')){
            var value = 'violation.'+row.cells[1].childNodes[1].value;
            arr.push(value);
            var ruleT = row.cells[2].childNodes[1].value;
            var ruleC = row.cells[3].children[0].children[0].value;
            var ruleA = row.cells[4].children[0].children[0].value;
            arr.push(ruleT);
            arr.push(ruleC);
            arr.push(ruleA);
        }
        array.push(arr);
    }
    //console.log(array);
    sendData(array);
  }

  function setPreview(){
    var table = document.getElementById('formTable');
    var preview = '';
    for (var i = 1; i < table.rows.length ;i++) {
        row = table.rows[i];
        var type = row.cells[0].childNodes[1].value;
        var tmp;
        if(!type.localeCompare('Text')){
            tmp = '\''+row.cells[1].children[0].children[0].value+'\' ';
            
        }else if(!type.localeCompare('Placeholder')){
            tmp = '<'+row.cells[1].childNodes[1].value+'> ';
        }else if(!type.localeCompare('Rule')){
            tmp = '('+row.cells[4].children[0].children[0].value+') ';
        }
        preview = preview + tmp;
    }
    var hPreview = document.getElementById('preview');
    hPreview.innerText = preview;
  }

  function editTemplate(){
    var test = document.getElementById('test');
    test.style.display = 'none';
    var edit = document.getElementById('edit');
    edit.style.display = '';
  }
  function testTemplate(){
    var test = document.getElementById('test');
    test.style.display = '';
    var edit = document.getElementById('edit');
    edit.style.display = 'none';
    
  }


  function readViolations(){
      readTextFile('violations.json', function(text){
        var jsonData = JSON.parse(text);
        /* for (var i = 0; i < jsonData.violations.length; i++) {
            var violation = jsonData.violations[i];
            console.log(violation.entity);
        } */
        //console.log(jsonData.violations.length);
        violations =  jsonData.violations;
      })
  }

  function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function testTemplates(){
      var feedback = document.getElementById('feedback_output');
      var argument = document.getElementById('argument_output');
      var suggestion = document.getElementById('suggestion_output');

      var randomViolation = violations[randomInteger(0, violations.length-1)];
      console.log(randomViolation)
      $.ajax({
        url: '/testTemplate',
        type: 'POST',
        data: {
            violation: randomViolation
        },
        success: function(resp){
            var response = resp.success;
            feedback.innerText = response.feedback;
            argument.innerText = response.argument;
            suggestion.innerText = response.suggestion;
            
        }
    });
  }