var JSONStream = require("JSONStream");
var es = require("event-stream");
var fs = require('fs');
var rosaenlgPug = require('rosaenlg');
var pluralize = require('pluralize')


var violation = {
    
  }



//il cibo per la query
//var entity = violation.entity;
//lingua per le label
var language = 'en';
//lingua per le consequences
var conseq_language = 'cons_en'

var timeout_promise = 50;

//label della entity
var entity_label;

//nutrienti positivi senza label
var raw_negative = [];
//nutrienti negativi senza label
var raw_positive = [];
//alternative senza label
var raw_alternatives = [];

//nutrienti positivi con label
var label_negative = [];
//nutrienti negativi con label
var label_positive = [];
//alternative con label
var label_alternatives = [];

//consequence positive (array a due dim)
var conseq_positive = [];
//consequence negative (array a due dim)
var conseq_negative = [];

//setta i nutrienti positivi (no label)
async function getPositive(){
    return new Promise(function(resolve, reject) {
        fileStream = fs.createReadStream('resources/properties.json', { encoding: "utf8" });
        fileStream.pipe(JSONStream.parse([entity, false, 'positive'])).pipe(
        es.through(function(data) {
            //console.log(data);
            raw_positive=data;
            //return data;
            resolve(data);
        })
        );
        setTimeout(() => resolve(null), timeout_promise)
    }) 
}
//setta i nutrienti negativi (no label)
 async function getNegative(){
    return new Promise(function(resolve, reject) {
        fileStream = fs.createReadStream('resources/properties.json', { encoding: "utf8" });
        fileStream.pipe(JSONStream.parse([entity, false, 'negative'])).pipe(
            es.through(function(data) {
                //console.log(data);
                raw_negative=data;
                resolve(data);
                //return data;
        
      })
    );
    setTimeout(() => resolve(null), timeout_promise)
    }) 
} 



    
//setta le alternative (no label)
async function getAlternative(){
    return new Promise(function(resolve, reject) {
        fileStream = fs.createReadStream('resources/properties.json', { encoding: "utf8" });
        fileStream.pipe(JSONStream.parse([entity, false, 'alternatives'])).pipe(
            es.through(function(data) {
                if(!data[data.length-1].localeCompare('Suggestion')){
                    data.pop();
                }
                //console.log(data);
                raw_alternatives=data;
                resolve(data);
                //return data;
        
      })
    );
    setTimeout(() => resolve(null), timeout_promise)
    }) 
    
}

//data una entity, restituisce la label
function getLabel(string, filename){
    return new Promise(function(resolve, reject) {
        fileStream = fs.createReadStream(filename, { encoding: "utf8" });
        fileStream.pipe(JSONStream.parse([string, false, true])).pipe(
        es.through(function(data) {
            if(!(data.langCode).localeCompare(language)){
                //console.log(data.label);
                //entity_label = data.label;
                resolve(data.label);
            }
            
            
        })
        );
        setTimeout(() => resolve(null), timeout_promise)
    }) 
}

//dato un nutrient(senza label), restituisce un array con le sue conseq (o null se non ne ha)
function getConseq(nutrient, filename, language ){
    return new Promise(function(resolve, reject) {
        fileStream = fs.createReadStream(filename, { encoding: "utf8" });
        fileStream.pipe(JSONStream.parse([nutrient, false , language, ])).pipe(
        es.through(function(data) {
            resolve(data);
        })
        );
        setTimeout(() => resolve(null), timeout_promise)
    }) 
}

function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function filter_array(test_array) {
    var index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        var value = test_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}

//ripulisce gli array dagli elementi vuoti e fa tutto in lowercase
function clean(){
    label_alternatives = filter_array(label_alternatives);
    label_positive = filter_array(label_positive);
    conseq_positive = filter_array(conseq_positive);
    label_negative = filter_array(label_negative);
    conseq_negative = filter_array(conseq_negative);
    entity_label = entity_label.toLowerCase();
    for(i=0; i<label_positive.length; i++){
        label_positive[i] = label_positive[i].toLowerCase();
    }
    for(i=0; i<label_negative.length; i++){
        label_negative[i] = label_negative[i].toLowerCase();
    }
    for(i=0; i<label_alternatives.length; i++){
        label_alternatives[i] = label_alternatives[i].toLowerCase();
    }
    
}
function reset(){
    raw_negative = [];
    raw_positive = [];
    raw_alternatives = [];
    
    label_negative = [];
    label_positive = [];
    label_alternatives = [];
    
    conseq_positive = [];
    conseq_negative = [];
}

async function main(input){
    reset();
    console.log(input);
    violation = input;
    entity = input.entity;
    entity_label = await getLabel(entity, 'resources/FoodCategoryList.json')
    await getPositive();
    await getNegative();
    await getAlternative();

    for (index = 0; index < raw_positive.length; index++) {
        label_positive[index] = await getLabel(raw_positive[index], 'resources/NutrientList.json');
        let tmp = await getConseq(raw_positive[index], 'resources/consequences.json', conseq_language);
        if(tmp!=null){
            if(tmp.length!=0){
                conseq_positive[index]  = await getConseq(raw_positive[index], 'resources/consequences.json', conseq_language);
            }else{
                console.log('empty found');
                label_positive.splice(index, 1);
            }
        }else{
            console.log('empty found');
            label_positive.splice(index, 1);
        }
        
        
    }

    for (index = 0; index < raw_negative.length; index++) {
        label_negative[index] = await getLabel(raw_negative[index], 'resources/NutrientList.json');
        let tmp = await getConseq(raw_negative[index], 'resources/consequences.json', conseq_language);
        if(tmp!=null){
            if(tmp.length!=0){
                conseq_negative[index]  = await getConseq(raw_negative[index], 'resources/consequences.json', conseq_language);
            }else{
                console.log('empty found');
                label_negative.splice(index, 1);
            }
        }else{
            console.log('empty found');
            label_negative.splice(index, 1);
        }
    }

    for (index = 0; index < raw_alternatives.length; index++) {
        label_alternatives[index]= await getLabel(raw_alternatives[index], 'resources/FoodCategoryList.json');
    }


    clean();

    console.log('Food: '+entity_label);
    console.log('Positive nutrients:');
    console.log(label_positive);
    console.log('Negative nutrients:');
    console.log(label_negative);
    console.log('Alternatives:');
    console.log(label_alternatives);
    console.log('Positive consequences:');
    console.log(conseq_positive);
    console.log('Negative consequences:');
    console.log(conseq_negative);

    
    violation.entity = entity_label;

    if(!violation.constraint.localeCompare('greater')){
        //positive nutrient
        if(label_positive.length!=0 && conseq_positive!=null){
            let nutrient_index = randomInteger(0, label_positive.length-1);
            console.log('chosen: '+conseq_positive[nutrient_index])
            let conseq_index = randomInteger(0,conseq_positive[nutrient_index].length-1);
            //console.log(nutrient_index + ' '+ conseq_index);
            //console.log(label_positive[nutrient_index] + ' '+ conseq_positive[nutrient_index][conseq_index]);
            violation.nutrient = label_positive[nutrient_index];
            violation.consequence = conseq_positive[nutrient_index][conseq_index];
        }else{
            violation.nutrient = 'null';
            violation.consequence = 'null';
        }
    }else{
        //negative nutrient
        if(label_negative.length!=0 && conseq_negative!=null){
            let nutrient_index = randomInteger(0, label_negative.length-1);
            console.log('chosen: '+conseq_negative[nutrient_index])
            let conseq_index = randomInteger(0,conseq_negative[nutrient_index].length-1);
            //console.log(nutrient_index + ' '+ conseq_index);
            //console.log(label_negative[nutrient_index] + ' '+ conseq_negative[nutrient_index][conseq_index]);
            violation.nutrient = label_negative[nutrient_index];
            violation.consequence = conseq_negative[nutrient_index][conseq_index];
        }else{
            violation.nutrient = 'null';
            violation.consequence = 'null';
        }
    }

    if(label_alternatives.length==0){
        violation.alternative = 'null';
    }else{
        violation.alternative = label_alternatives[randomInteger(0, label_alternatives.length-1)];
    }
    //console.log(violation.alternative);

    console.log(violation);
    var toReturn = [];

    try {
        if (fs.existsSync('js/feedback.pug')) {
            let res = rosaenlgPug.renderFile('js/feedback.pug', {
                language: 'en_US',
                violation: violation,
                pluralize: pluralize
            });
            //console.log( res );
            toReturn.push(res);
        }else{
            toReturn.push('Feedback template not found');
        }
      } catch(err) {
        console.error(err)
      }

      try {
        if (fs.existsSync('js/argument.pug')) {
            let res = rosaenlgPug.renderFile('js/argument.pug', {
                language: 'en_US',
                violation: violation
            });
            //console.log( res );
            toReturn.push(res);
        }else{
            toReturn.push('Argument template not found')
        }
      } catch(err) {
        console.error(err)
      }

    try {
    if (fs.existsSync('js/suggestion.pug')) {
        res = rosaenlgPug.renderFile('js/suggestion.pug', {
            language: 'en_US',
            violation: violation
        });
        //console.log( res );
        toReturn.push(res);
    }else{
        toReturn.push('Suggestion template not found')
    }
    } catch(err) {
    console.error(err)
    }

    return toReturn;
}

//------------------
//main();
module.exports = {main};


  


  