
var violation = {
  "entityType": "violation.entityType",
  "entity": "violation.entity",
  "timing": "violation.timing",
  "quantity": "violation.quantity",
  "expectedQuantity": "violation.expectedQuantity",
  "constraint": "violation.constraint",
  "nutrient": "violation.nutrient",
  "consequence": "violation.consequence",
  "alternative": "violation.alternative",
};

var ruleType = {
  SINGULAR: 1,
  PLURAL: 2,
  BEGINSWITH: 3,
  EQUALS: 4,
  NOTEQUALS: 5,
}

function placeholder(variable){
  return '|!{ '+ variable + ' } \n';
}

function rule(variable, condition, output, isOutputEntity, type){
  let tmp;
  switch(type){
    case ruleType.EQUALS:
      tmp = 'if ' + variable + ' == \'' + condition + '\'';
      break;
    case ruleType.NOTEQUALS:
      tmp = 'if ' + variable + ' != \'' + condition + '\'';
      break;
    case ruleType.SINGULAR:
      tmp = '- if (pluralize.isSingular('+variable+') )';
      break;
    case ruleType.PLURAL:
      tmp = '- if (pluralize.isPlural('+variable+') )';
      break;
    case ruleType.BEGINSWITH:
      tmp = '- if ('+variable+'.startsWith('+condition+') )';
      break;
  }
  
  tmp = tmp + '\n\t';
  if(isOutputEntity){
    tmp = tmp + '|'+ placeholder(output);
  }else{
    tmp = tmp + '|'+output+'\n';
  }

  return tmp;
}

function text(string){
  return '|'+string+'\n';
}


function save(data, language){
  
  var language_folder = language + '/';
  var toReturn = '';
  for(var i = 1; i < data.length; i++) {
    var row = data[i];
    if(!row[0].localeCompare('Text')){
      toReturn = toReturn+text(row[1]);
    }else if(!row[0].localeCompare('Placeholder')){
      toReturn = toReturn+placeholder(row[1]);
    }else if(!row[0].localeCompare('Rule')){
      var rType = row[2];
      var variable = row[1];
      var condition = row[3];
      var output = row[4];
      if(!rType.localeCompare('equals')){
        toReturn = toReturn+rule(variable,condition,output, false, ruleType.EQUALS);
      }else if(!rType.localeCompare('notEquals')){
        toReturn = toReturn+rule(variable,condition,output, false, ruleType.NOTEQUALS);
      }else if(!rType.localeCompare('beginsWith')){
        toReturn = toReturn+rule(variable,condition,output, false, ruleType.BEGINSWITH);
      }else if(!rType.localeCompare('isSingular')){
        toReturn = toReturn+rule(variable,condition,output, false, ruleType.SINGULAR);
      }else if(!rType.localeCompare('isPlural')){
        toReturn = toReturn+rule(variable,condition,output, false, ruleType.PLURAL);
      }else if(!rType.localeCompare('isNull')){
        toReturn = toReturn+rule(variable,'null',output, false, ruleType.EQUALS);
      }
    }

    
  }
  var filename = data[0].toLowerCase();
  //console.log(filename);
  var fs = require('fs');
  fs.writeFile('js/'+language_folder+filename+".pug", toReturn,function(err) {
      if (err) {
          console.log(err);
      }
  }); 
  return toReturn;
}



module.exports = {text, placeholder, rule, save};




/* console.log(rule(violation.constraint, 'less', violation.entity, true));

var final = '|Did you know that\n'+rule(violation.constraint, 'less', violation.entity, true);


var fs = require('fs');
fs.writeFile("tuto.pug", final,function(err) {
    if (err) {
        console.log(err);
    }
});  */

//feedback template
/* var feedback = [];
feedback.push(rule(violation.timing, 'Week', 'This week', false, ruleType.EQUALS));
feedback.push(rule(violation.timing, 'Day', 'Today', false, ruleType.EQUALS));
feedback.push(rule(violation.constraint, 'less', 'you consumed a lot of', false, ruleType.EQUALS));
feedback.push(rule(violation.constraint, 'greater', 'you didn\'t consume enough', false, ruleType.EQUALS));
feedback.push(placeholder(violation.entity));
feedback.push(text('.'));

var feedback_out = '';

for(i=0; i<feedback.length; i++){
  feedback_out = feedback_out + feedback[i];
}

var fs = require('fs');
fs.writeFile("feedback.pug", feedback_out,function(err) {
    if (err) {
        console.log(err);
    }
}); 


//argument template
var argument = [];
argument.push(text('did you know that'));
argument.push(placeholder(violation.entity));
argument.push(text('contain'));
argument.push(placeholder(violation.nutrient));
//argument.push(rule(violation.constraint, 'less', 'that can cause', false, ruleType.EQUALS));
//argument.push(rule(violation.constraint, 'greater', 'that can help', false, ruleType.EQUALS));
argument.push(placeholder(violation.consequence));
argument.push(text('?'));

var argument_out = '';

for(i=0; i<argument.length; i++){
  argument_out = argument_out + argument[i];
}

var fs = require('fs');
fs.writeFile("argument.pug", argument_out,function(err) {
    if (err) {
        console.log(err);
    }
}); 


//suggestion template
var suggestion = [];
suggestion.push(text('next time try with some'));
suggestion.push(placeholder(violation.alternative));
suggestion.push(text('.'));

var suggestion_out = '';

for(i=0; i<suggestion.length; i++){
  suggestion_out = suggestion_out + suggestion[i];
}

var fs = require('fs');
fs.writeFile("suggestion.pug", suggestion_out,function(err) {
    if (err) {
        console.log(err);
    }
}); 

console.log(ruleType);
 */

