/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twig;

import com.twig.template.core.exceptions.TwigTemplateException;
import com.twig.template.core.processor.TwigTemplateDriver;
import com.twig.template.core.util.FileProcess;

/**
 *
 * @author loghi
 */
public class Twig {

    //resources/testfiles/test-templates.json è il file dove si definiscono i template
    private static final String TEST_TEMPLATE_FILE = "resources/testfiles/test-templates.json";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String jsonInput = FileProcess.readFile("resources/testfiles/feedback1.json");
		String tempDef = FileProcess.readFile(TEST_TEMPLATE_FILE);
		TwigTemplateDriver driver = new TwigTemplateDriver(tempDef);
		String o;
		try {
			o = driver.getNLGFromJsonString(jsonInput);
			System.out.println(o);
		} catch (TwigTemplateException e) {
			e.printStackTrace();
		}
    }
    
}
